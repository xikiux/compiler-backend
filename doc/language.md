# The intermediate code language specification

Comments:
A comment is an '@', followed by text and ending with a new line
```
@this is a comment
this is not a comment because it is on a new line without an '@'
```

Register declarations: There are 3 kinds of registers.
* Variable registers - a generic register, any use
* Argument registers - a register for passing arguments
* Return registers   - a register for storing return values

All registers have an 'N' at the end specifying any number.
All registers must be declared in numerical order from 0 up.

```
	@variable register
	rvN

	@argument register
	raN

	@return register
	rrN

	@Examples
	rv0
	ra0
	rr0
	,
	rv1
	ra1
	rr1
	.
	.
	.
	rvN
	raN
	rrN
```

Function declarations: Functions have one name, take N arguments and return N values
```
@main function, takes no arguments, prints an unsigned integer to console, returns an unsigned 64bit integer
function main() (u64) {
	rv0 = u64 10;
	rv1 += u64 12;
	ra0 = rv1;
	rv2 = call convert.u64_to_char_string;
	ra0 = rv2;
	rv3 = call print_to_console;
	ra0 = rv3;
	call return;
}
```

TODO: add more details and improve language specification.