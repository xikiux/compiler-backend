# A compiler backend for linux compilers

A full compiler covers these stages:
* Lexical analysis
* Parsing
* Abstracting (Semantic Amalysis)
* Intermediating
* Assembly code generation
* Binary Generation

This backend expects intermediate code and will cover these stages:
* Assembly code generation
* Binary Generation

Note: This will not make faster compilers, this will make compilers easier to make.
If you want a fast compiler then you should implement the backend yourself, it will save you performance in the long run.
But if you are trying to write a compiler for the x86_64 architecture quickly, then this project if for you.

# How to include in project

Just drop the folder "compiler-backend" into your project folder and include the file "compiler-backend/src/backend.hpp".
Don't forget to adjust the include path so it's relative to the file your using it in!