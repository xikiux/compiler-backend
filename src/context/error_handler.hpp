#pragma once

#include <iostream>
#include <vector>

namespace compiler_backend {
	namespace error_handling {
		class error {
		public:
			std::string m_error_type;
			std::string m_error_msg;

			error(std::string error_type, std::string msg) {
				m_error_type = error_type;
				m_error_msg = msg;
			}
		};

		class error_handler {
			std::vector<error*>* m_errors;

		public:
			error_handler() {
				m_errors = new std::vector<error*>();
			}

			bool has_error_occcured() {
				if (m_errors->size() > 0) {
					return true;
				} else {
					return false;
				}
			}

			void log_error(error* error) {
				m_errors->push_back(error);
			}
		};
	}
}