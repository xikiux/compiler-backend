#pragma once

#include <iostream>
#include <vector>

#include "error_handler.hpp"

namespace compiler_backend {
	class context {
	public:
		error_handling::error_handler* m_error_handler;

		context() {
			m_error_handler = new error_handling::error_handler();
		}
	};
}