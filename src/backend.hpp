#pragma once

#include <vector>

#include "stages/assembly_generation.hpp"
#include "stages/elf_file_generation.hpp"
#include "context/context.hpp"

namespace compiler_backend {
	class backend {
		context* m_context;
		assembly_generator* m_asm_gen;
		elf_generator* m_elf_gen;

	public:
		backend() {
			m_context = new context();
			m_asm_gen = new assembly_generator();
			m_elf_gen = new elf_generator();
		}

		std::vector<uint8_t>* generate_executable(std::string& intermediate_code) {
			
		}
	};
}